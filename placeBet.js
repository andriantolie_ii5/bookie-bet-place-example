const _ = require("lodash");
const {  PrivateKey, TransactionBuilder } = require("peerplaysjs-lib");
const { Apis, ChainConfig } = require("peerplaysjs-ws")

// Connect to the blockchain
Apis.instance("wss://peerplays-dev.blocktrades.info/ws", true)
.init_promise.then((res) => {
    console.log("connected to network");
    // It's going to be unknown, so set prefix manually
    ChainConfig.setPrefix("PPY");
    // Place bet
    placeBet();
});

const placeBet = () => {
    const transaction = new TransactionBuilder();
    const operationParams = {
        "fee": {
            "amount": 0,
            "asset_id": 0
        },
        "bettor_id": "1.2.7",
        "betting_market_id": "1.21.0",
        "amount_to_bet": {
            "amount": 100000,
            "asset_id": "1.3.0"
        },
        "backer_multiplier": 13400,
        "amount_reserved_for_fees": 1000,
        "back_or_lay": "back"
    };
    const operationType = "bet_place";
    transaction.add_type_operation(operationType, operationParams);

    const keys = {
        owner: PrivateKey.fromWif("5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"),
        active: PrivateKey.fromWif("5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3"),
        memo: PrivateKey.fromWif("5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3")
    };

    // Get stored private keys and public keys
    let privateKeyWifsByRole = {};
    let publicKeyStringsByRole = {};

    _.forEach(keys, (privateKey, role) => {
        privateKeyWifsByRole[role] = privateKey.toWif();
        publicKeyStringsByRole[role] = privateKey.toPublicKey().toPublicKeyString();
    });

    // Set required fees
    transaction.set_required_fees().then(() => {
      console.log('Set required fees');
      // Get potential signatures
      // Inside, it"s trying to ask the blockchain based on the seller account id attached in the transaction
        return transaction.get_potential_signatures();
    }).then(( result ) => {
        console.log('Set potential public keys');
        const potentialPublicKeys = result.pubkeys;
        // Check if none of the potential public keys is equal to our public keys
        const myPubKeys = _.filter(publicKeyStringsByRole, publicKey => potentialPublicKeys.includes(publicKey));
        if (myPubKeys.length === 0) {
            throw new Error("No valid Signatures");
        }
        // Filter potential signatures to get required keys needed to sign the transaction
        return transaction.get_required_signatures(myPubKeys);
    }).then((requiredPublicKeys) => {
        console.log('Set required public keys');
        // Add required keys to the transaction
        _.forEach(requiredPublicKeys, (requiredPublicKey) => {
            const role = _.findKey(publicKeyStringsByRole, (publicKey) => publicKey === requiredPublicKey);
            // Get private key pair
            const requiredPrivateKey = PrivateKey.fromWif(privateKeyWifsByRole[role]);
            // Add signature
            transaction.add_signer(requiredPrivateKey, requiredPublicKey);
        });
        // Broadcast transaction
        return transaction.broadcast();
    }).then(() => {
        console.log("Processing Transactions succeeds");
    }).catch((error) => {
        console.error("Processing Transaction fails", error);
    });
};
