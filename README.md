# Bookie Bet Place Example #

Both peerplaysjs-ws and peerplayjs-lib in this repo is referring to the repo https://bitbucket.org/peerplaysblockchain/peerplays-graphenejs-ws and https://bitbucket.org/peerplaysblockchain/peerplays-graphenejs-lib branch betting2.
I modified their package.json slightly, so their package name is peerplaysjs-ws and peerplaysjs-lib instead of bitsharesjs-ws and bitsharesjs to avoid confusion.
No other change are made to the libraries.


```
cd lib/peerplaysjs-ws
npm install
cd ../peerplaysjs-lib
npm install
npm link ../peerplayjs-ws
cd ../..
npm install
npm link lib/peerplaysjs-ws lib/peerplaysjs-lib
node placeBet
```